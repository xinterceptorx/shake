project {
  modelVersion '4.0.0'
  groupId 'online.interceptor'
  artifactId 'shake'
  version '0.1-SNAPSHOT'
  name 'shake'
  url 'http://maven.apache.org'
  properties {
    'project.build.sourceEncoding' 'UTF-8'
  }
  dependencies {
    dependency {
      groupId 'junit'
      artifactId 'junit'
      version '4.12'
      scope 'test'
    }
  }
  build {
    pluginManagement {
      plugins {
        plugin {
          artifactId 'maven-compiler-plugin'
          version '3.6.1'
        }
        plugin {
          artifactId 'maven-source-plugin'
          version '3.0.1'
        }
        plugin {
          artifactId 'maven-surefire-plugin'
          version '2.19.1'
        }
        plugin {
          artifactId 'maven-clean-plugin'
          version '3.0.0'
        }
        plugin {
          artifactId 'maven-resources-plugin'
          version '3.0.2'
        }
        plugin {
          artifactId 'maven-install-plugin'
          version '2.5.2'
        }
        plugin {
          artifactId 'maven-jar-plugin'
          version '3.0.2'
        }
        plugin {
          artifactId 'maven-deploy-plugin'
          version '2.8.2'
        }
        plugin {
          artifactId 'maven-site-plugin'
          version '3.6'
        }
      }
    }
    plugins {
      plugin {
        artifactId 'maven-compiler-plugin'
        configuration {
          source '1.8'
          target '1.8'
        }
      }
      plugin {
        artifactId 'maven-clean-plugin'
        executions {
          execution {
            id 'auto-clean'
            phase 'initialize'
            goals {
              goal 'clean'
            }
          }
        }
      }
      plugin {
        artifactId 'maven-surefire-plugin'
        configuration {
          parallel 'methods'
          threadCount '10'
        }
      }
    }
  }
}
