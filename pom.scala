import org.sonatype.maven.polyglot.scala.model._
import scala.collection.immutable.Seq

Model(
  "online.interceptor" % "shake" % "0.1-SNAPSHOT",
  name = "shake",
  url = "http://maven.apache.org",
  dependencies = Seq(
    "junit" % "junit" % "4.12" % "test"
  ),
  properties = Map(
    "project.build.sourceEncoding" -> "UTF-8"
  ),
  build = Build(
    pluginManagement = PluginManagement(
      plugins = Seq(
        Plugin(
          "org.apache.maven.plugins" % "maven-compiler-plugin" % "3.6.1"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-source-plugin" % "3.0.1"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-surefire-plugin" % "2.19.1"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-clean-plugin" % "3.0.0"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-resources-plugin" % "3.0.2"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-install-plugin" % "2.5.2"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-jar-plugin" % "3.0.2"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-deploy-plugin" % "2.8.2"
        ),
        Plugin(
          "org.apache.maven.plugins" % "maven-site-plugin" % "3.6"
        )
      )
    ),
    plugins = Seq(
      Plugin(
        "org.apache.maven.plugins" % "maven-compiler-plugin",
        configuration = Config(
          source = "1.8",
          target = "1.8"
        )
      ),
      Plugin(
        "org.apache.maven.plugins" % "maven-clean-plugin",
        executions = Seq(
          Execution(
            id = "auto-clean",
            phase = "initialize",
            goals = Seq(
              "clean"
            )
          )
        )
      ),
      Plugin(
        "org.apache.maven.plugins" % "maven-surefire-plugin",
        configuration = Config(
          parallel = "methods",
          threadCount = "10"
        )
      )
    )
  ),
  modelVersion = "4.0.0"
)
